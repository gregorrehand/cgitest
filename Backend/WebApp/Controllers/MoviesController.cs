﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    
    [ApiController]

    public class MoviesController : Controller
    {
        private static readonly Repository _repository = new Repository();
        private readonly Service _service = new Service(_repository);


        [HttpGet]
        public IEnumerable<Movie> GetMovies()
        {
            return _service.ReturnListOfMovies();
        }
        [HttpGet("{id}")]
        public  Movie GetMovie(int id)
        {
            return _service.GetMovieById(id);
        }
    }
}

