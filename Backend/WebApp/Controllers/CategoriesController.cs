﻿using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    
    [ApiController]

    public class CategoriesController : Controller
    {
        static readonly Repository _repository = new Repository();
        private readonly Service _service = new Service(_repository);


        [HttpGet]
        public IEnumerable<Category> GetCategories()
        {
            return _service.ReturnListOfCategories();
        }
    }
}