﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain
{
    public class Service
    {
        private readonly Repository _repository;

        public Service(Repository repository)
        {
            _repository = repository;
        }

        public  List<Movie> ReturnListOfMovies()
        {
            return  _repository.ReturnMovies();
        }

        public Movie GetMovieById(int id)
        {
            return _repository.ReturnMovieById(id);
        }
        public  List<Category> ReturnListOfCategories()
        {
            return  _repository.ReturnCategories();
        }
    }
}