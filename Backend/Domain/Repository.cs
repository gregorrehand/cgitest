﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class Repository
    {
        private readonly MoviesList _moviesList = new MoviesList();

        public List<Movie> ReturnMovies()
        {
            return _moviesList.Movies;
        }
        public List<Category> ReturnCategories()
        {
            return _moviesList.Categories;
        }

        public Movie ReturnMovieById(int id)
        {
            foreach (var movie in _moviesList.Movies.Where(movie => movie.Id == id))
            {
                return movie;
            }

            throw new ArgumentException("Movie with given id not found!");
        }
    }
}