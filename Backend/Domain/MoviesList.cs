﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Domain
{
    public class MoviesList
    {
        public readonly List<Movie> Movies;
        public readonly List<Category> Categories;
        public MoviesList()
        {
            Movies = new List<Movie>();
            Categories = new List<Category>
            {
                new Category {Id = 0, Name = "Crime"},
                new Category {Id = 1, Name = "Thriller"},
                new Category {Id = 2, Name = "Drama"}
            };
            Movies.Add(new Movie
            {
                Id = 0,
                Title = "The Irishman",
                Year = 2019,
                Rating = 7.9,
                Description = "A Mobster story",
                CategoryId = 0,
                Category = Categories[0]
            });
            Movies.Add(new Movie
            {
                Id = 1,
                Title = "Parasite",
                Year = 2019,
                Rating = 8.3,
                Description = "Great film",
                CategoryId = 1,
                Category = Categories[1]
            });
            Movies.Add(new Movie
            {
                Id = 2,
                Title = "Pulp Fiction",
                Year = 1994,
                Rating = 8.4,
                Description = "A masterpiece",
                CategoryId = 0,
                Category = Categories[0]
            });
            Movies.Add(new Movie
            {
                Id = 3,
                Title = "Mid90s",
                Year = 2019,
                Rating = 7.8,
                Description = "Lovely",
                CategoryId = 2,
                Category = Categories[2]
            });
        }
    }
}