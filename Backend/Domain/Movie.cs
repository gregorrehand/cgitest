﻿using System.Data.Common;

namespace Domain
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; } = default!;
        public int Year { get; set; }
        public string Description { get; set; } = default!;
        public double Rating { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; } = default!;
        
        // Vajalik frontendis detailide kuvamiseks
        public string IdAsString => "id" + Id.ToString();

    }
}