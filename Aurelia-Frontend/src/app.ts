import { IMovie } from "IMovie";
import { MovieService } from "movie-service";
import { inject, observable } from 'aurelia-framework';
import { ICategory } from "ICategory";
import { CategoryService } from "category-service";


export class App {
  private _movies: IMovie[] = [];
  public selectedMovie: IMovie;
  private _movieService: MovieService
  private _categoryService: CategoryService
  private _checkBoxes: string[] = [];

  private _categories: ICategory[] = [];
  customLabel = IMovie => IMovie.title;
  @observable query: string;

  selectedCategories = [];

  constructor() {
      this._movieService = new MovieService();
      this._categoryService = new CategoryService();
      this.query = "";
  }
  
  attached() {
      this._movieService.getMovies().then(
          data => this._movies = data,
      );
      this._categoryService.getCategories().then(
        data => this._categories = data,
    );
  }

  // Autocomplete otsingu lahendus võetud https://medium.com/@jintoppy/creating-an-autocomplete-with-aurelia-ba8614ae37dc
  queryChanged(){
    console.log(this.selectedMovie);

    if (this.selectedCategories.length > 0) {
      this._movieService.getMovies().then(
        data => this._movies = data.filter(data => this.selectedCategories.includes(data.category.name) && data.title.toLowerCase().includes(this.query.toLowerCase()))
      );
    }
    else {
      this._movieService.getMovies().then(
        data => this._movies = data.filter(data => data.title.toLowerCase().includes(this.query.toLowerCase()))  
    );  
    }
  }
  getDetails(id: string) {
    console.log(id);
    this._movieService.getMovie(parseInt(id)).then(
      data => this.selectedMovie = data
    );
    console.log(this.selectedMovie);
  }
}
