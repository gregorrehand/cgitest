import { ICategory } from 'ICategory';
import { autoinject } from 'aurelia-framework';
import { IMovie } from 'IMovie';
import {HttpClient} from 'aurelia-fetch-client';


@autoinject
export class CategoryService {
    constructor() {
    }

    private readonly _baseUrl = 'https://localhost:5001/api/categories';
    private httpClient = new HttpClient;


    async getCategories(): Promise<ICategory[]> {
      try {
        const response = await this.httpClient
          .fetch(this._baseUrl, { cache: "no-store" });
        const data = await response.json();
        return data;
      }
      catch (reason) {
        console.error(reason);
        return [];
      }

  }

  }
