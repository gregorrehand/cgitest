import { ICategory } from "ICategory";

export interface IMovie {
  id: number;
  title: string;
  description: string;
  year: number;
  rating: number;
  categoryId: number;
  category: ICategory;
  idAsString: string;
}
