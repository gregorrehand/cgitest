import { autoinject } from 'aurelia-framework';
import { IMovie } from 'IMovie';
import {HttpClient} from 'aurelia-fetch-client';


@autoinject
export class MovieService {
    constructor() {
    }

    private readonly _baseUrl = 'https://localhost:5001/api/movies';
    private httpClient = new HttpClient;


    async getMovies(): Promise<IMovie[]> {
      try {
        const response = await this.httpClient
          .fetch(this._baseUrl, { cache: "no-store" });
        const data = await response.json();
        return data;
      }
      catch (reason) {
        console.error(reason);
        return [];
      }

  }

  async getMovie(id: number): Promise<IMovie | null> {
      try {
      const response = await this.httpClient
        .fetch(this._baseUrl + '/' + id, { cache: "no-store" });
      const data = await response.json();
      return data;
    }
    catch (reason) {
      console.error(reason);
      return null;
    }
  }

  }
